# Discord Notifier
### Configuring `.env`

In the `.env` file:

Set the `GENERAL_SERVER_ID` to the channel you want the bot to reply to
Set the `SERVER_ID` to the server's id the bot belongs to
Set the `TOKEN` to bot's token from discord

TO RUN 
```
$ cd root_directory
$ pipenv install
$ pipenv shell
$ pip install -r requirments.txt
$ python3 main.py
```