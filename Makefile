deploy:
	# run as a (background) service
	docker-compose down && docker-compose build && docker-compose up -d && docker-compose logs -f


up:
	# run as a (background) service
	docker-compose down && docker-compose up -d && docker-compose logs -f

down:
	# run as a service and attach to it
	docker-compose down