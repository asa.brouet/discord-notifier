import re
import unittest
def replace_nums(value, words_to_replace, replace_with='A'):
    big_regex = re.compile('|'.join(map(re.escape, words_to_replace)))
    return big_regex.sub(replace_with, value)

    # return ''.join([replace_with if s in words_to_replace else s for s in value.split()])
class Test(unittest.TestCase):
    def testMe(self):
        result = "you have .. ending in the options are: account ending in 15328, account ending in 9051, account ending in 7625."
        result2 = replace_nums(result, words_to_replace=['15328', '9051', '7625'])
        print(result2)
        self.assertIn('account ending in 1528', result)
        self.assertIn('account ending in 9051', result)
        self.assertIn('account ending in 7625', result)
        # self.assertEqual('you have .. ending in the options are: account ending in AAAA, account ending in AAAA, account ending in AAAA.', result2)
if __name__ == '__main__':
  unittest.main()
