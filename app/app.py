import discord
import os
from discord.ext.commands import Bot
from discord import Member
from discord.ext import commands
from dotenv import load_dotenv
from pylite.simplite import Pylite
from dotenv import load_dotenv

load_dotenv()
BOT_PREFIX = '!'
Intents = discord.Intents.default()
Intents.members = True
Intents.presences = True
client = discord.Client(intents=Intents)
bot = commands.Bot(command_prefix=BOT_PREFIX, Intents=Intents)
general_channel_id = int(os.getenv('GENERAL_CHANNEL_ID'))
server_id = int(os.getenv('SERVER_ID'))
db = Pylite('discord')
db.add_table('opt_in_update_user', member_id='text', member_name='text')


@client.event
async def on_ready():
    id = db.query('SELECT * FROM opt_in_update_user WHERE id = 1')
    print(id)
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_member_update(prev, cur):
    channel = client.get_channel(general_channel_id)
    # await channel.send('WE are playing today')
    games = ["rocket league", "apex legends"]
    # make sure game titles are lowercase
    mention_id = ''
    if cur.activity and cur.activity.name.lower() in games:
        for guild in client.guilds:
            for member in guild.members:
                if member.activity and cur.activity.name.lower() not in str(member.activity).lower():
                    mention_id = mention_id + '<@' + str(member.id) + '>, '
                elif not member.activity:
                    mention_id = mention_id + '<@' + str(member.id) + '>, '

        mention_id.lstrip(", ")
        if mention_id:
            message = mention_id + str(cur) + " is playing " + cur.activity.name.lower()
            print(message)
            await channel.send(message)
        print('We are playing something ' + cur.activity.name.lower())


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content == '!add_me':
        id = db.query('SELECT member_id FROM opt_in_update_user WHERE member_id LIKE ' + str(message.author.id))
        print(id)
        if id:
            print("You already exist", id)
        else:
            new = db.insert('opt_in_update_user', str(message.author.id), str(message.author.name))
            print("Added you", new)

client.run(os.getenv('TOKEN'))
